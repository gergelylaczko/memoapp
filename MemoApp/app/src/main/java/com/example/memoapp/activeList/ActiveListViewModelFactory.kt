package com.example.memoapp.activeList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.memoapp.database.MemoDatabaseDao
import java.lang.IllegalArgumentException

class ActiveListViewModelFactory(private val dataSource: MemoDatabaseDao): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(ActiveListViewModel::class.java))
            return ActiveListViewModel(dataSource) as T
        throw IllegalArgumentException("unknown viewmodel class")
    }

}