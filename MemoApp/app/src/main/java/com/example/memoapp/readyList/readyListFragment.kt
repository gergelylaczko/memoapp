package com.example.memoapp.readyList


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.memoapp.R
import com.example.memoapp.database.MemoDatabase
import com.example.memoapp.databinding.FragmentReadyListBinding
import com.example.memoapp.memoList.MemoDataAdapter
import com.example.memoapp.memoList.MemoDataListener


class readyListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentReadyListBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_ready_list, container, false)
        binding.setLifecycleOwner(this)

        val application = requireNotNull(this.activity).application
        val dataSource = MemoDatabase.getInstance(application).memoDatabaseDao
        val viewModelFactory = ReadyListViewModelFactory(dataSource)
        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(ReadyListViewModel::class.java)

        val adapter = MemoDataAdapter(MemoDataListener { id, title ->
            this.findNavController()
                .navigate(readyListFragmentDirections.actionReadyListFragmentToDetailsFragment(id, title))
        })
        binding.memoList.adapter = adapter

        viewModel.memoList.observe(this, Observer {
            it?.let { adapter.submitList(it) }
        })

        return binding.root
    }


}
