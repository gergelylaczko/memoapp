package com.example.memoapp.addScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.memoapp.database.MemoDatabaseDao
import java.lang.IllegalArgumentException

class AddViewModelFactory(private val dataSource: MemoDatabaseDao): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddViewModel::class.java)){
            return AddViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown view model class.")
    }
}