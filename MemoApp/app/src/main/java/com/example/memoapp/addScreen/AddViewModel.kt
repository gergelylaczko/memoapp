package com.example.memoapp.addScreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabaseDao
import kotlinx.coroutines.*

class AddViewModel(dataSource: MemoDatabaseDao) : ViewModel() {
    val database = dataSource
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun saveMemo(memo: MemoData){
        uiScope.launch {
            insert(memo)
        }
    }

    private suspend fun insert(memo: MemoData){
        withContext(Dispatchers.IO){
            database.insert(memo)
        }
    }
}