package com.example.memoapp.memoEdit


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.example.memoapp.R
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabase
import com.example.memoapp.databinding.FragmentMemoEditBinding
import com.example.memoapp.memoList.DateUtils
import java.util.*


class memoEditFragment : Fragment() {

    private lateinit var binding: FragmentMemoEditBinding
    private lateinit var viewModel: EditViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_memo_edit, container, false)
        binding.setLifecycleOwner(this)

        val application = requireNotNull(this.activity).application
        val arguments = memoEditFragmentArgs.fromBundle(arguments!!)
        val dataSource = MemoDatabase.getInstance(application).memoDatabaseDao
        val viewModelFactory = EditViewModelFactory(dataSource, arguments.memoId)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EditViewModel::class.java)

        binding.editViewModel = viewModel
        binding.dateUtils = DateUtils


        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        binding.pickDateTxt.setOnClickListener {
            val dialog = DatePickerDialog(this.context!!, DatePickerDialog.OnDateSetListener { view, year, month, day ->
                binding.pickDateTxt.setText(String.format(getString(R.string.datepicker_text), year, month + 1, day))
            }, year, month, day)

            dialog.show()
        }

        binding.pickTimeTxt.setOnClickListener {
            val tpd = TimePickerDialog(this.context!!, TimePickerDialog.OnTimeSetListener { view, hour, minute ->
                binding.pickTimeTxt.setText(String.format(getString(R.string.timepicker_text), hour, minute))
            }, hour, minute, true)

            tpd.show()
        }

        viewModel.memo.observe(this, Observer { memo ->
            viewModel.updateReadyStatus(memo.ready)
        })

        binding.btnSave.setOnClickListener {
            updateMemo()
        }

        viewModel.memoUpdated.observe(this, Observer {
            if (it == true) {
                Toast.makeText(context, getString(R.string.edit_success_message), Toast.LENGTH_LONG).show()
                this.findNavController().navigate(memoEditFragmentDirections.actionMemoEditFragmentToMemoListFragment())
                viewModel.doneUpdateFinished()
            }
        })

        return binding.root
    }


    fun updateMemo() {
        val title = binding.editTitle.text.toString()
        val text = binding.editText.text.toString()
        val date = binding.pickDateTxt.text.toString()
        val time = binding.pickTimeTxt.text.toString()

        if (title != "" && text != "" && date != "" && time != "") {
            val calendar = Calendar.getInstance()
            val dateParts = date.split("/")
            val timeParts = time.split(":")
            calendar.set(
                dateParts[0].toInt(),
                dateParts[1].toInt() - 1,
                dateParts[2].toInt(),
                timeParts[0].toInt(),
                timeParts[1].toInt()
            )
            viewModel.updateMemo(title, text, calendar.time)
        } else {
            Toast.makeText(context, getString(R.string.edit_error_message), Toast.LENGTH_LONG).show()
        }
    }


}
