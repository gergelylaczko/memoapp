package com.example.memoapp.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import java.util.*


@Dao
interface MemoDatabaseDao {

    @Query("SELECT * FROM memo_table ORDER BY date ASC")
    fun getAllMemos(): LiveData<List<MemoData>>


    @Insert
    fun insert(memo: MemoData)


    @Query("SELECT * FROM memo_table WHERE id = :id")
    fun getMemoById(id: Long): LiveData<MemoData>


    @Query("SELECT * FROM memo_table WHERE ready = 1 ORDER BY date ASC")
    fun getReadyMemos(): LiveData<List<MemoData>>


    @Query("SELECT * FROM memo_table WHERE ready = :ready AND date BETWEEN :from AND :to ORDER BY date ASC")
    fun getFilteredMemos(ready: Boolean, from: Date, to: Date): LiveData<List<MemoData>>


    @Query("SELECT * FROM memo_table WHERE date BETWEEN :from AND :to ORDER BY date ASC")
    fun getFilteredMemosByDate(from: Date, to: Date): LiveData<List<MemoData>>


    @Query("SELECT * FROM memo_table WHERE ready = 0 AND date < :current ORDER BY date ASC")
    fun getExpiredMemos(current: Date): LiveData<List<MemoData>>


    @Query("DELETE FROM memo_table WHERE id = :id")
    fun deleteMemoById(id: Long)


    @Query("UPDATE memo_table SET title = :title, text = :text, date = :date, ready = :ready WHERE id = :id")
    fun updateMemo(id: Long, title: String, text: String, date: Date, ready: Boolean)


    @Query("UPDATE memo_table SET ready = :ready WHERE id = :id")
    fun updateMemoReadyStatus(id: Long, ready: Boolean)


    @Query("DELETE FROM memo_table")
    fun deleteAll()

    @Query("SELECT * FROM memo_table WHERE title LIKE :title ORDER BY date ASC")
    fun searchMemos(title: String): LiveData<List<MemoData>>

    @Query("SELECT * FROM memo_table WHERE ready = 0 AND date > :current")
    fun getActiveMemos(current: Date): LiveData<List<MemoData>>
}