package com.example.memoapp.addScreen

import android.app.Application
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.memoapp.R
import com.example.memoapp.databinding.AddFragmentBinding
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.example.memoapp.database.Converters
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabase
import kotlinx.android.synthetic.main.add_fragment.*
import java.util.*

class addFragment : Fragment() {
    private lateinit var binding: AddFragmentBinding
    private lateinit var viewModel: AddViewModel
    private lateinit var application: Application
    private lateinit var memo: MemoData

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.add_fragment, container, false)
        binding.setLifecycleOwner(this)

        //val application= requireNotNull(this.activity).application
        application = requireNotNull(this.activity).application
        val dataSource = MemoDatabase.getInstance(application).memoDatabaseDao
        val viewModelFactory = AddViewModelFactory(dataSource)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AddViewModel::class.java)

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        binding.pickDateTxt.setOnClickListener {
            val dialog = DatePickerDialog(this.context!!, DatePickerDialog.OnDateSetListener { view, year, month, day ->
                binding.pickDateTxt.setText(String.format(getString(R.string.datepicker_text), year, month + 1, day))
            }, year, month, day)

            dialog.show()
        }

        binding.pickTimeTxt.setOnClickListener {
            val tpd = TimePickerDialog(this.context!!, TimePickerDialog.OnTimeSetListener { view, hour, minute ->
                binding.pickTimeTxt.setText(String.format(getString(R.string.timepicker_text), hour, minute))
            }, hour, minute, true)

            tpd.show()
        }

        binding.btnSave.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_addFragment_to_memoListFragment)
            insertToDb()

        }

        return binding.root
    }

    fun insertToDb() {
        val title = binding.editTitle.text.toString()
        val text = binding.editText.text.toString()
        val date = binding.pickDateTxt.text.toString()
        val time = binding.pickTimeTxt.text.toString()
        val calendar = Calendar.getInstance()

        if (title != "" && text != "" && date != "" && time != "") {
            val parts = date.split("/")
            val hParts = time.split(":")
            calendar.set(parts[0].toInt(), parts[1].toInt() - 1, parts[2].toInt(), hParts[0].toInt(), hParts[1].toInt())
            memo = MemoData(title = title, text = text, date = calendar.time)
            viewModel.saveMemo(memo)
            Toast.makeText(application, "Sikeres mentés.", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(application, "Sikertelen mentés.", Toast.LENGTH_LONG).show()
        }
    }
}