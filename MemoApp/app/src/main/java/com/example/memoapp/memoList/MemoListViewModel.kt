package com.example.memoapp.memoList

import androidx.lifecycle.ViewModel
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabaseDao
import kotlinx.coroutines.*
import java.util.*


class MemoListViewModel(dataSource: MemoDatabaseDao) : ViewModel() {

    private val database = dataSource
    var memoList = database.getAllMemos()


    fun filterMemoList(active: Boolean, ready: Boolean, from: Date?, to: Date?) {
        val fromDate = if (from == null) Date(Long.MIN_VALUE) else from
        val toDate = if (to == null) Date(Long.MAX_VALUE) else to

        if(active == true && ready == false) {
            memoList = database.getFilteredMemos(false, fromDate, toDate)
            return
        }
        if(active == false && ready == true) {
            memoList = database.getFilteredMemos(true, fromDate, toDate)
            return
        }
        memoList = database.getFilteredMemosByDate(fromDate, toDate)
    }

    fun resetListFilter() {
        memoList = database.getAllMemos()
    }

    fun searchMemos(word: String){
        if(word!="")
            memoList=database.searchMemos("%"+word+"%")
        else
            memoList=database.getAllMemos()
    }
}