package com.example.memoapp.memoList

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.example.memoapp.R
import com.example.memoapp.databinding.DialogFilterBinding
import com.example.memoapp.memoDetails.DeleteDialogFragment
import java.lang.IllegalStateException
import java.util.*


class FilterDialogFragment : DialogFragment() {

    internal lateinit var listener: FilterDialogListener
    private lateinit var binding: DialogFilterBinding

    interface FilterDialogListener {
        fun onFilterPositiveClick(dialog: DialogFragment, active: Boolean, ready: Boolean, from: Date?, to: Date?)
        fun onFilterNegativeClick(dialog: DialogFragment)
        fun onFilterNeutralClick(dialog: DialogFragment)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        listener = targetFragment as FilterDialogFragment.FilterDialogListener

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_filter, null, false)

            binding.pickDateFrom.setOnClickListener {
                setDate(it)
            }
            binding.pickDateTo.setOnClickListener {
                setDate(it)
            }


            builder.setView(binding.root)
                .setPositiveButton(
                    R.string.filter_dialog_positive,
                    DialogInterface.OnClickListener { dialog, id ->
                        val from = prepareDate(binding.pickDateFrom.text.toString())
                        val to = prepareDate(binding.pickDateTo.text.toString())
                        listener.onFilterPositiveClick(
                            this,
                            binding.chkActive.isChecked,
                            binding.chkReady.isChecked,
                            from,
                            to
                        )
                    })
                .setNegativeButton(
                    R.string.filter_dialog_negative,
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onFilterNegativeClick(this)
                    })
                .setNeutralButton(
                    R.string.filter_dialog_neutral,
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onFilterNeutralClick(this)
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null.")
    }

    private fun setDate(view: View) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        val txtView = view as TextView

        val dialog = DatePickerDialog(this.context!!, DatePickerDialog.OnDateSetListener { view, year, month, day ->
            txtView.setText(String.format(getString(R.string.datepicker_text), year, month + 1, day))
        }, year, month, day)

        dialog.show()
    }

    private fun prepareDate(dateString: String): Date? {
        if (dateString == "") {
            return null
        }
        val calendar = Calendar.getInstance()
        val dateParts = dateString.split("/")

        calendar.set(
            dateParts[0].toInt(),
            dateParts[1].toInt() - 1,
            dateParts[2].toInt()
        )
        return calendar.time
    }
}