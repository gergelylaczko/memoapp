package com.example.memoapp.readyList

import androidx.lifecycle.ViewModel
import com.example.memoapp.database.MemoDatabaseDao

class ReadyListViewModel(dataSource: MemoDatabaseDao) : ViewModel() {

    private val database = dataSource
    val memoList = database.getReadyMemos()

}