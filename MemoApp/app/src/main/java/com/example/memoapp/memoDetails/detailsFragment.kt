package com.example.memoapp.memoDetails


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.memoapp.R
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabase
import com.example.memoapp.databinding.FragmentDetailsBinding
import com.example.memoapp.memoList.DateUtils

class detailsFragment : Fragment(), DeleteDialogFragment.DeleteDialogListener {

    private lateinit var binding: FragmentDetailsBinding
    private lateinit var viewModel: DetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        binding.setLifecycleOwner(this)

        val application = requireNotNull(this.activity).application
        val arguments = detailsFragmentArgs.fromBundle(arguments!!)
        val dataSource = MemoDatabase.getInstance(application).memoDatabaseDao
        val viewModelFactory = DetailsViewModelFactory(arguments.memoId, dataSource)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsViewModel::class.java)

        binding.dateUtils = DateUtils
        binding.detailsViewModel = viewModel

        viewModel.memoDeletionSuccessful.observe(this, Observer {
            if (it == true) {
                Toast.makeText(context, getString(R.string.delete_memo_successful), Toast.LENGTH_LONG).show()
                this.findNavController().navigate(detailsFragmentDirections.actionDetailsFragmentToMemoListFragment())
                viewModel.doneMemoDeletionSuccessful()
            }
        })

        binding.btnDelete.setOnClickListener {
            deleteMemo()
        }

        viewModel.navigateToMemoEdit.observe(this, Observer {
            if (it != null) {
                this.findNavController().navigate(detailsFragmentDirections.actionDetailsFragmentToMemoEditFragment(it))
                viewModel.doneNavigatingToMemoEdit()
            }
        })

        return binding.root
    }


    private fun deleteMemo() {
        val dialog = DeleteDialogFragment()
        dialog.setTargetFragment(this, 0)
        dialog.show(fragmentManager!!, "delete_dialog")
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        viewModel.deleteMemo()
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {}
}
