package com.example.memoapp.memoDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.memoapp.database.MemoDatabaseDao
import java.lang.IllegalArgumentException


class DetailsViewModelFactory(private val memoId: Long, private val dataSource: MemoDatabaseDao) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            return DetailsViewModel(memoId, dataSource) as T
        }
        throw IllegalArgumentException("Unknown view model class.")
    }
}