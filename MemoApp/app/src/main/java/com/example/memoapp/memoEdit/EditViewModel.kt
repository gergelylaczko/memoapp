package com.example.memoapp.memoEdit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabaseDao
import kotlinx.coroutines.*
import java.util.*


class EditViewModel(val dataSource: MemoDatabaseDao, private val memoId: Long) : ViewModel() {

    private val database = dataSource
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var memo: LiveData<MemoData>

    private val _readyStatus = MutableLiveData<Boolean>()
    val readyStatus: LiveData<Boolean>
        get() = _readyStatus

    init {
        memo = database.getMemoById(memoId)
    }

    fun updateReadyStatus(value: Boolean) {
        _readyStatus.value = value
    }


    fun updateMemo(title: String, text: String, date: Date) {
        uiScope.launch {
            val ready = readyStatus.value!!
            update(title, text, date, ready)
            updateFinished()
        }
    }

    private suspend fun update(title: String, text: String, date: Date, ready: Boolean) {
        withContext(Dispatchers.IO) {
            val id = memo.value!!.id
            database.updateMemo(id, title, text, date, ready)
        }
    }


    private val _memoUpdated = MutableLiveData<Boolean?>()
    val memoUpdated: LiveData<Boolean?>
        get() = _memoUpdated

    private fun updateFinished() {
        _memoUpdated.value = true
    }

    fun doneUpdateFinished() {
        _memoUpdated.value = null
    }
}