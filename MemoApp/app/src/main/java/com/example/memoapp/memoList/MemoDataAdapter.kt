package com.example.memoapp.memoList

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.memoapp.R
import com.example.memoapp.database.MemoData
import com.example.memoapp.databinding.ListItemBinding
import java.util.*


class MemoDataAdapter(val clickListener: MemoDataListener) :
    ListAdapter<MemoData, MemoDataAdapter.ViewHolder>(MemoDataDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val memo = getItem(position)
        holder.bind(memo, clickListener)
    }


    class ViewHolder private constructor(val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(memo: MemoData, clickListener: MemoDataListener) {
            binding.memo = memo
            binding.clickListener = clickListener
            binding.dateUtils = DateUtils

            setViewHolderBackground(memo)

            binding.executePendingBindings()
        }

        private fun setViewHolderBackground(memo: MemoData) {
            if (memo.ready == true) {
                binding.root.setBackgroundResource(R.drawable.list_item_ready)
                binding.textShortDay.setBackgroundResource(R.drawable.list_item_ready_secondary)
            } else if (memo.date.compareTo(Calendar.getInstance().time) < 0) {
                binding.root.setBackgroundResource(R.drawable.list_item_expired)
                binding.textShortDay.setBackgroundResource(R.drawable.list_item_expired_secondary)
            } else {
                binding.root.setBackgroundResource(R.drawable.list_item_active)
                binding.textShortDay.setBackgroundResource(R.drawable.list_item_active_secondary)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class MemoDataDiffCallback : DiffUtil.ItemCallback<MemoData>() {

    override fun areContentsTheSame(oldItem: MemoData, newItem: MemoData): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: MemoData, newItem: MemoData): Boolean {
        return oldItem.id == newItem.id
    }

}


class MemoDataListener(val clickListener: (id: Long, title: String) -> Unit) {
    fun onClick(memo: MemoData) = clickListener(memo.id, memo.title)
}

