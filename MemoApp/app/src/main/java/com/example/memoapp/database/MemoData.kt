package com.example.memoapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull
import java.util.*


@Entity(tableName = "memo_table")
data class MemoData(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,


    @NotNull
    @ColumnInfo(name = "title")
    var title: String,


    @NotNull
    @ColumnInfo(name = "text")
    var text: String,


    @NotNull
    @ColumnInfo(name = "date")
    var date: Date,


    @ColumnInfo(name = "ready")
    var ready: Boolean = false
)
