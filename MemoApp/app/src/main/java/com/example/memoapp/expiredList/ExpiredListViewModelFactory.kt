package com.example.memoapp.expiredList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.memoapp.database.MemoDatabaseDao
import java.lang.IllegalArgumentException


class ExpiredListViewModelFactory(private val dataSource: MemoDatabaseDao) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExpiredListViewModel::class.java)) {
            return ExpiredListViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown view model class.")
    }
}