package com.example.memoapp.mainScreen


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController

import com.example.memoapp.R
import com.example.memoapp.databinding.MainScreenFragmentBinding
import com.example.memoapp.memoList.MemoDataAdapter
import com.example.memoapp.memoList.MemoDataListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar


class mainScreenFragment : Fragment() {

    private lateinit var binding: MainScreenFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding=DataBindingUtil.inflate(inflater,R.layout.main_screen_fragment, container, false)
        binding.setLifecycleOwner(this)

        binding.fab.setOnClickListener{view: View ->
            view.findNavController().navigate(R.id.action_mainScreenFragment_to_addFragment)
        }

        binding.btnToList.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_mainScreenFragment_to_memoListFragment)
        }

        binding.btnToReadyList.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_mainScreenFragment_to_readyListFragment)
        }

        binding.btnToActiveList.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_mainScreenFragment_to_activeListFragment)
        }

        binding.btnToExpiredList.setOnClickListener { view: View ->
            view.findNavController().navigate(R.id.action_mainScreenFragment_to_expiredListFragment)
        }

        return binding.root
    }


}
