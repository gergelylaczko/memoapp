package com.example.memoapp.expiredList

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabaseDao
import java.util.*

class ExpiredListViewModel(dataSource: MemoDatabaseDao) : ViewModel() {

    private val database = dataSource
    lateinit var memoList: LiveData<List<MemoData>>

    init {
        val calendar = Calendar.getInstance()
        memoList = database.getExpiredMemos(calendar.time)
    }
}