package com.example.memoapp.memoDetails

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.example.memoapp.R
import java.lang.ClassCastException
import java.lang.IllegalStateException


class DeleteDialogFragment : DialogFragment() {

    internal lateinit var listener: DeleteDialogListener

    interface DeleteDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment)
        fun onDialogNegativeClick(dialog: DialogFragment)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        listener = targetFragment as DeleteDialogListener
        return activity?.let {
            val builder = AlertDialog.Builder(it, R.style.AlertDialogTheme)

            builder.setTitle(R.string.delete_dialog_title)
            builder.setMessage(R.string.delete_dialog_message)
                .setPositiveButton(R.string.delete_dialog_positive,
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onDialogPositiveClick(this)
                    })
                .setNegativeButton(R.string.delete_dialog_negative,
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onDialogNegativeClick(this)
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null.")
    }
}