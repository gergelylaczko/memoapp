package com.example.memoapp.memoList

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun formatDate(date: Date?): String {
        if (date != null) {
            return SimpleDateFormat("yyyy. MM. dd. HH:mm").format(date)
        }
        return ""
    }

    fun formatDateShortDay(date: Date): String {
        return SimpleDateFormat("EEE").format(date)
    }


    fun formatDateForEdit(date: Date?): String {
        if (date != null) {
            return SimpleDateFormat("yyyy/MM/dd").format(date)
        }
        return ""
    }

    fun formatTimeForEdit(date: Date?): String {
        if (date != null) {
            return SimpleDateFormat("HH:mm").format(date)
        }
        return ""
    }
}