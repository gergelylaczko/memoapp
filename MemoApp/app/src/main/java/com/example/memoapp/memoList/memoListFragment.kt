package com.example.memoapp.memoList


import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.memoapp.R
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabase
import com.example.memoapp.databinding.FragmentMemoListBinding
import com.example.memoapp.memoDetails.DeleteDialogFragment
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*


class memoListFragment : Fragment(), FilterDialogFragment.FilterDialogListener {

    private lateinit var viewModel: MemoListViewModel
    private lateinit var adapter: MemoDataAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentMemoListBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_memo_list, container, false)
        binding.setLifecycleOwner(this)

        val application = requireNotNull(this.activity).application
        val dataSource = MemoDatabase.getInstance(application).memoDatabaseDao
        val viewModelFactory = MemoListViewModelFactory(dataSource)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MemoListViewModel::class.java)

        adapter = MemoDataAdapter(MemoDataListener { memoId, title ->
            this.findNavController()
                .navigate(memoListFragmentDirections.actionMemoListFragmentToDetailsFragment(memoId, title))
        })
        binding.memoList.adapter = adapter

        setHasOptionsMenu(true)

        updateDataObserver()

        binding.btnAdd.setOnClickListener {
            this.findNavController().navigate(memoListFragmentDirections.actionMemoListFragmentToAddFragment())
        }

        binding.searchBox.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(s: String): Boolean {
                viewModel.searchMemos(s)
                updateDataObserver()
                return true
            }

            override fun onQueryTextSubmit(s: String): Boolean {
                val intent = Intent(application, memoListFragment::class.java)
                intent.putExtra(SearchManager.QUERY, s)
                intent.setAction(Intent.ACTION_SEARCH)
                startActivity(intent)
                return true
            }
        })



        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.filter -> openFilter()
        }

        return super.onOptionsItemSelected(item)
    }


    private fun openFilter() {
        val dialog = FilterDialogFragment()
        dialog.setTargetFragment(this, 0)
        dialog.show(fragmentManager!!, "filter_dialog")
    }

    private fun updateDataObserver() {
        viewModel.memoList.observe(this, Observer {
            it?.let { adapter.submitList(it) }
        })
    }

    override fun onFilterPositiveClick(
        dialog: DialogFragment,
        active: Boolean,
        ready: Boolean,
        from: Date?,
        to: Date?
    ) {
        viewModel.filterMemoList(active, ready, from, to)
        updateDataObserver()
    }

    override fun onFilterNegativeClick(dialog: DialogFragment) {
    }

    override fun onFilterNeutralClick(dialog: DialogFragment) {
        viewModel.resetListFilter()
        updateDataObserver()
    }
}
