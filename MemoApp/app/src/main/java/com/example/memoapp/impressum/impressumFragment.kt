package com.example.memoapp.impressum


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil

import com.example.memoapp.R
import com.example.memoapp.databinding.ImpressumFragmentBinding


class impressumFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: ImpressumFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.impressum_fragment, container, false)
        binding.setLifecycleOwner(this)

        binding.webview.webViewClient=object: WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }

        binding.webview!!.loadUrl("file:///android_asset/impressumHtml.html")

        return binding.root
    }


}
