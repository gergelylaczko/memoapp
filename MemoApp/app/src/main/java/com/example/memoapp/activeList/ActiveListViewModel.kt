package com.example.memoapp.activeList

import android.icu.util.Calendar
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabaseDao

@RequiresApi(Build.VERSION_CODES.N)
class ActiveListViewModel(dataSource: MemoDatabaseDao): ViewModel() {
    private val database=dataSource
    lateinit var memoList: LiveData<List<MemoData>>
    init {
        val c = Calendar.getInstance()
        memoList=database.getActiveMemos(c.time)
    }
}