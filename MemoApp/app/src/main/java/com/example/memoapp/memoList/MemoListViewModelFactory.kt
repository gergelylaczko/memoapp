package com.example.memoapp.memoList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.memoapp.database.MemoDatabaseDao
import java.lang.IllegalArgumentException

class MemoListViewModelFactory(private val dataSource: MemoDatabaseDao) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MemoListViewModel::class.java)) {
            return MemoListViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown view model class.")
    }
}