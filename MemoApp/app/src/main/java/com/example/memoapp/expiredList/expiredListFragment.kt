package com.example.memoapp.expiredList


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.memoapp.R
import com.example.memoapp.database.MemoDatabase
import com.example.memoapp.databinding.FragmentExpiredListBinding
import com.example.memoapp.memoList.MemoDataAdapter
import com.example.memoapp.memoList.MemoDataListener


class expiredListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentExpiredListBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_expired_list, container, false)
        binding.setLifecycleOwner(this)

        val application = requireNotNull(this.activity).application
        val dataSource = MemoDatabase.getInstance(application).memoDatabaseDao
        val viewModelFactory = ExpiredListViewModelFactory(dataSource)
        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(ExpiredListViewModel::class.java)

        val adapter = MemoDataAdapter(MemoDataListener { id, title ->
            this.findNavController()
                .navigate(expiredListFragmentDirections.actionExpiredListFragmentToDetailsFragment(id, title))
        })
        binding.memoList.adapter = adapter

        viewModel.memoList.observe(this, Observer {
            it?.let { adapter.submitList(it) }
        })

        return binding.root
    }


}
