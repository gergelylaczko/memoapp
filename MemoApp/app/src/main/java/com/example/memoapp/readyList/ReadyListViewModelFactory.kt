package com.example.memoapp.readyList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.memoapp.database.MemoDatabaseDao
import java.lang.IllegalArgumentException


class ReadyListViewModelFactory(private val dataSource: MemoDatabaseDao) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ReadyListViewModel::class.java)) {
            return ReadyListViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown view model class.")
    }
}