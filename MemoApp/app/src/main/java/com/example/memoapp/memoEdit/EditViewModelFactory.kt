package com.example.memoapp.memoEdit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.memoapp.database.MemoDatabaseDao
import java.lang.IllegalArgumentException


class EditViewModelFactory(val dataSource: MemoDatabaseDao, private val memoId: Long) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EditViewModel::class.java)) {
            return EditViewModel(dataSource, memoId) as T
        }
        throw IllegalArgumentException("Unknown view model class.")
    }
}