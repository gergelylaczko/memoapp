package com.example.memoapp.memoDetails

import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.memoapp.database.MemoData
import com.example.memoapp.database.MemoDatabaseDao
import kotlinx.coroutines.*


class DetailsViewModel(private val memoId: Long, val dataSource: MemoDatabaseDao) : ViewModel() {


    val database = dataSource
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var memo: LiveData<MemoData>

    init {
        memo = database.getMemoById(memoId)
    }

    val readyLabel = Transformations.map(memo, {
        when (it.ready) {
            true -> "BEFEJEZETT"
            false -> "AKTÍV"
        }
    })

    fun setMemoStatusToReady(memo: MemoData) {
        uiScope.launch {
            updateReadyStatus(memo.id, true)
        }
    }

    fun setMemoStatusToActive(memo: MemoData) {
        uiScope.launch {
            updateReadyStatus(memo.id, false)
        }
    }

    private suspend fun updateReadyStatus(memoId: Long, status: Boolean) {
        withContext(Dispatchers.IO) {
            database.updateMemoReadyStatus(memoId, status)
        }
    }


    fun deleteMemo() {
        val id = memo.value!!.id
        uiScope.launch {
            delete(id)
            enableMemoDeletionSuccessful()
        }
    }

    private suspend fun delete(id: Long) {
        withContext(Dispatchers.IO) {
            database.deleteMemoById(id)
        }
    }

    private val _memoDeletionSuccessful = MutableLiveData<Boolean?>()
    val memoDeletionSuccessful: LiveData<Boolean?>
        get() = _memoDeletionSuccessful

    private fun enableMemoDeletionSuccessful() {
        _memoDeletionSuccessful.value = true
    }

    fun doneMemoDeletionSuccessful() {
        _memoDeletionSuccessful.value = null
    }

    private val _navigateToMemoEdit = MutableLiveData<Long?>()
    val navigateToMemoEdit: LiveData<Long?>
        get() = _navigateToMemoEdit

    fun startNavigatingToMemoEdit(memo: MemoData) {
        _navigateToMemoEdit.value = memo.id
    }

    fun doneNavigatingToMemoEdit() {
        _navigateToMemoEdit.value = null
    }
}